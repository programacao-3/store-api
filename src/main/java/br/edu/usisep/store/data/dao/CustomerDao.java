package br.edu.usisep.store.data.dao;



import br.edu.usisep.store.data.entity.Customer;
import br.edu.usisep.store.data.hibernate.HibernateSessionFactory;

import java.util.List;

public class CustomerDao {

    public List<Customer> findAll() {
        var session = HibernateSessionFactory.getSession();

        var query = session.createQuery("from Customer", Customer.class);
        var result = query.list();

        session.close();

        return result;
    }

    public Customer findById(Integer id) {
        var session = HibernateSessionFactory.getSession();

        var query = session.createQuery("from Customer where id = :pId", Customer.class);
        query.setParameter("pId", id);
        var result = query.uniqueResult();

        session.close();

        return result;
    }

    public void save(Customer customer) {
        var session = HibernateSessionFactory.getSession();
        var transaction = session.beginTransaction();

        try {
            session.save(customer);
            transaction.commit();
        } catch(Exception error) {
            error.printStackTrace();
            transaction.rollback();
        }

        session.close();
    }

    public void delete(Customer customer) {
        var session = HibernateSessionFactory.getSession();
        var transaction = session.beginTransaction();

        try {
            session.delete(customer);
            transaction.commit();
        } catch(Exception error) {
            error.printStackTrace();
            transaction.rollback();
        }

        session.close();
    }

}



package br.edu.usisep.store.controller;



import br.edu.usisep.store.domain.dto.CustomerDto;
import br.edu.usisep.store.domain.dto.RegisterCustomerDto;
import br.edu.usisep.store.domain.usecase.FindAllCustomersUseCase;
import br.edu.usisep.store.domain.usecase.FindCustomerByIdUseCase;
import br.edu.usisep.store.domain.usecase.RegisterCustomerUseCase;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @GetMapping
    public ResponseEntity<List<CustomerDto>> findAll() {
        var useCase = new FindAllCustomersUseCase();
        var result = useCase.execute();

        if (result.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CustomerDto> findById(@PathVariable("id") Integer id) {
        var useCase = new FindCustomerByIdUseCase();

        var customer = useCase.execute(id);

        if (customer == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(customer);
    }

    @PostMapping
    public ResponseEntity<Boolean> save(@RequestBody RegisterCustomerDto customer) {
        var useCase = new RegisterCustomerUseCase();
        useCase.execute(customer);

        return ResponseEntity.ok(true);
    }

}

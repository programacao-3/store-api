package br.edu.usisep.store.controller;

import br.edu.usisep.store.domain.dto.ProductsDto;
import br.edu.usisep.store.domain.dto.RegisterProductsDto;
import br.edu.usisep.store.domain.usecase.FindAllProductsUseCase;
import br.edu.usisep.store.domain.usecase.FindProductsByldUseCase;
import br.edu.usisep.store.domain.usecase.RegisterProductsUseCase;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductsController {

    @GetMapping
    public ResponseEntity<List<ProductsDto>> findAll() {
        var useCase = new FindAllProductsUseCase();
        var result = useCase.execute();

        if (result.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductsDto> findById(@PathVariable("id") Integer id) {
        var useCase = new FindProductsByldUseCase();

        var product = useCase.execute(id);

        if (product == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(product);
    }

    @PostMapping
    public ResponseEntity<Boolean> save(@RequestBody RegisterProductsDto product) {
        var useCase = new RegisterProductsUseCase();
        useCase.execute(product);

        return ResponseEntity.ok(true);
    }

}







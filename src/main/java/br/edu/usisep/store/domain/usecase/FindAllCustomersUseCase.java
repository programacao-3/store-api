package br.edu.usisep.store.domain.usecase;



import br.edu.usisep.store.data.dao.CustomerDao;
import br.edu.usisep.store.domain.builder.CustomerBuilder;
import br.edu.usisep.store.domain.dto.CustomerDto;

import java.util.List;

public class FindAllCustomersUseCase {

    public List<CustomerDto> execute() {
        var dao = new CustomerDao();
        var customers = dao.findAll();

        var builder = new CustomerBuilder();
        return builder.from(customers);
    }

}

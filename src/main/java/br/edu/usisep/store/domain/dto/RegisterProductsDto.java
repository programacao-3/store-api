package br.edu.usisep.store.domain.dto;

import lombok.Data;

@Data
public class RegisterProductsDto {

    private String name;

    private String description;

    private Integer price;

    private String brand;

    private Integer status;

}

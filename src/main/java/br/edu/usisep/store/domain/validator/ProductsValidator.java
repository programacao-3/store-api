package br.edu.usisep.store.domain.validator;

import br.edu.usisep.store.domain.dto.RegisterProductsDto;
import org.apache.commons.lang3.Validate;

public class ProductsValidator {

    public void validate(RegisterProductsDto registerProducts) {
        Validate.notBlank(registerProducts.getName(), "Informe o nome do produto!");
        Validate.notBlank(registerProducts.getDescription(), "Informe a descrição!");
        Validate.notNull(registerProducts.getPrice(), "Informe o valor!");
        Validate.notBlank(registerProducts.getBrand(), "Informe a marca!");
        Validate.notNull(registerProducts.getStatus(), "Informe o status!");
    }

}

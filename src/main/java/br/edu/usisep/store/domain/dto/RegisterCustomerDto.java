package br.edu.usisep.store.domain.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RegisterCustomerDto {

    private String name;
    private String email;

    private LocalDate birthday;

    private String cpf;

}
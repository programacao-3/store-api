package br.edu.usisep.store.domain.validator;

import org.apache.commons.lang3.Validate;

public class FindCustomerByIdValidator {

    public void validate(Integer id) {
        Validate.isTrue(id > 0, "Informe o id do cliente!");
    }

}

package br.edu.usisep.store.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ProductsDto {

    private final Integer id;

    private final String name;

    private final String description;

    private final Integer price;

    private final String brand;

    private final Integer status;

}



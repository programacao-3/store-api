package br.edu.usisep.store.domain.usecase;

import br.edu.usisep.store.data.dao.ProductsDao;
import br.edu.usisep.store.domain.builder.ProductsBuilder;
import br.edu.usisep.store.domain.dto.RegisterProductsDto;
import br.edu.usisep.store.domain.validator.ProductsValidator;

public class RegisterProductsUseCase {

    public void execute(RegisterProductsDto registerProducts) {
        var validator = new ProductsValidator();
        validator.validate(registerProducts);

        var builder = new ProductsBuilder();
        var products = builder.from(registerProducts);

        var dao = new ProductsDao();
        dao.save(products);
    }

}

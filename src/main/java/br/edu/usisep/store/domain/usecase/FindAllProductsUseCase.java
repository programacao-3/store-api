package br.edu.usisep.store.domain.usecase;

import br.edu.usisep.store.data.dao.ProductsDao;
import br.edu.usisep.store.domain.builder.ProductsBuilder;
import br.edu.usisep.store.domain.dto.ProductsDto;

import java.util.List;

public class FindAllProductsUseCase {

    public List<ProductsDto> execute() {
        var dao = new ProductsDao();
        var product = dao.findAll();

        var builder = new ProductsBuilder();
        return builder.from(product);
    }
}





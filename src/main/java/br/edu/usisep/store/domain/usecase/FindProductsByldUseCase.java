package br.edu.usisep.store.domain.usecase;

import br.edu.usisep.store.data.dao.ProductsDao;
import br.edu.usisep.store.domain.builder.ProductsBuilder;
import br.edu.usisep.store.domain.dto.ProductsDto;
import br.edu.usisep.store.domain.validator.FindProductsByidValidator;

public class FindProductsByldUseCase {

    public ProductsDto execute(Integer id) throws IllegalArgumentException {
        var validator = new FindProductsByidValidator();
        validator.validate(id);

        var dao = new ProductsDao();
        var products = dao.findById(id);

        var builder = new ProductsBuilder();
        return builder.from(products);
    }

}

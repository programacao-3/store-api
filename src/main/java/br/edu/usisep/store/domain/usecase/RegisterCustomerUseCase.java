package br.edu.usisep.store.domain.usecase;


import br.edu.usisep.store.data.dao.CustomerDao;
import br.edu.usisep.store.domain.builder.CustomerBuilder;
import br.edu.usisep.store.domain.dto.RegisterCustomerDto;
import br.edu.usisep.store.domain.validator.CustomerValidator;

public class RegisterCustomerUseCase {

    public void execute(RegisterCustomerDto registerCustomer) {
        var validator = new CustomerValidator();
        validator.validate(registerCustomer);

        var builder = new CustomerBuilder();
        var customer = builder.from(registerCustomer);

        var dao = new CustomerDao();
        dao.save(customer);
    }

}

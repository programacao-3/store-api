package br.edu.usisep.store.domain.builder;
import br.edu.usisep.store.data.entity.Products;
import br.edu.usisep.store.domain.dto.ProductsDto;
import br.edu.usisep.store.domain.dto.RegisterProductsDto;

import java.util.List;
import java.util.stream.Collectors;

public class ProductsBuilder {


    public List<ProductsDto> from(List<Products> products) {
        return products.stream().map(this::from).collect(Collectors.toList());
    }

    public ProductsDto from(Products products) {

            return new ProductsDto(
                    products.getId(),
                    products.getName(),
                    products.getDescription(),
                    products.getPrice(),
                    products.getBrand(),
                    products.getStatus()
            );
        }


    public Products from(RegisterProductsDto registerProducts) {
        var products = new Products();
        products.setName(registerProducts.getName());
        products.setDescription(registerProducts.getDescription());
        products.setPrice(registerProducts.getPrice());
        products.setBrand(registerProducts.getBrand());
        products.setStatus(registerProducts.getStatus());

        return products;
    }

}





